function ImageSlider(element, options){
  var self;
  var Element;
  var imageInterval;
  var currentIndex = 0;
  var Options = {
    images: [],
    duration: 6000
  }


  self = Object.defineProperties({},{
    'element': {
      get: function(){ return Element; },
      set: function(value) {
        if(typeof value == 'string'){
          Element = document.querySelector(value);
        }
        else if(value instanceof HTMLElement) {
          Element = value;
        }
      }
    },
    'images': {
      get: function(){
        return Options.images;
      },
      set: function(value){
        Options.images = value;
      }
    },
    'duration': {
      get: function(){
        return Options.duration;
      },
      set: function(value){
        Options.duration = value;
      }
    }
  });

  self.element = element;

  Object.keys(options).forEach(function(key){
    if(Options[key]) Options[key] = options[key];
  });

  function build(){
    Element.innerHTML = '';
    Element.classList.add('image-slider');
    Options.images.forEach(function(image){
      var el = document.createElement('div');
      el.setAttribute('style', 'background-image: url(' + image + ')');
      Element.appendChild(el);
    });

    start();
  }

  function start(){
    clearInterval(imageInterval);
    resetActive();
    Element.children[currentIndex].classList.add('active');
    increaseIndex();
    imageInterval = setInterval(function(){
      resetActive();
      Element.children[currentIndex].classList.add('active');
      increaseIndex();
    },Options.duration);
  }

  function resetActive(){
    [].slice.call(Element.children).forEach(function(el){
      el.classList.remove('active');
    })
  }

  function increaseIndex(){
    if(currentIndex == Options.images.length - 1) currentIndex = 0;
    else currentIndex++;
  }

  build();

  return self;
};
